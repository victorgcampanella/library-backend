import * as Yup from 'yup';
import Book from '../models/Book';
import User from '../models/User';

class BookController {
  async index(resquest, response) {
    const books = await Book.findAll();

    return response.json(books);
  }

  async store(request, response) {
    const checkUserProvider = await User.findOne({
      where: { id: request.userId, admin: true },
    });

    if (!checkUserProvider) {
      return response.status(401).json({ error: 'User is not an admin' });
    }

    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(request.body))) {
      return response.status(400).json({ error: 'Validation fails' });
    }

    const bookExists = await Book.findOne({
      where: { name: request.body.name },
    });

    if (bookExists) {
      return response.status(400).json({ error: 'Book already exists' });
    }

    const nameOfBook = request.body;

    const book = {
      name: nameOfBook.name,
      comments: [],
    };

    const { id, name } = await Book.create(book);

    return response.json({
      id,
      name,
    });
  }

  async update(request, response) {
    const checkUserProvider = await User.findOne({
      where: { id: request.userId, admin: true },
    });

    if (!checkUserProvider) {
      return response.status(401).json({ error: 'User is not an admin' });
    }

    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(request.body))) {
      return response.status(400).json({ error: 'Validation fails' });
    }

    const book = await Book.findByPk(request.params.id);

    const { id, name } = await book.update(request.body);

    return response.json({
      id,
      name,
    });
  }

  async delete(request, response) {
    const checkUserProvider = await User.findOne({
      where: { id: request.userId, admin: true },
    });

    if (!checkUserProvider) {
      return response.status(401).json({ error: 'User is not an admin' });
    }

    const book = await Book.findByPk(request.params.id);

    book.destroy();

    return response.send();
  }
}

export default new BookController();
