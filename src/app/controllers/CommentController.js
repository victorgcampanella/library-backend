import Book from '../models/Book';

class CommentController {
  async store(request, response) {
    const book = await Book.findByPk(request.params.id);

    const { comment } = request.body;

    const { comments } = book.dataValues;

    const commentArray = comments;

    commentArray.push(comment);

    const newComment = {
      comments: commentArray,
    };

    await book.update(newComment);

    return response.json({
      message: 'Your comment has been added',
    });
  }
}

export default new CommentController();
