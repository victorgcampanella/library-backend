import Sequelize, { Model } from 'sequelize';

class Book extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        comments: Sequelize.ARRAY(Sequelize.STRING),
      },
      { sequelize }
    );
  }
}

export default Book;
